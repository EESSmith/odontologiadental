﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OdontologiaDental.Migrations
{
    public partial class Dental : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Consultorio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Consultorio = table.Column<string>(nullable: true),
                    Direccion = table.Column<string>(nullable: true),
                    Encargado = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Consultorio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dispositivo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Mac = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Serie = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dispositivo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permisos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Estado = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permisos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sesope",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ATM = table.Column<string>(nullable: true),
                    ATM_A_D = table.Column<string>(nullable: true),
                    ATM_A_I = table.Column<string>(nullable: true),
                    ATM_C_D = table.Column<string>(nullable: true),
                    ATM_C_I = table.Column<string>(nullable: true),
                    Amigdalas = table.Column<string>(nullable: true),
                    Cara = table.Column<string>(nullable: true),
                    Carrillos = table.Column<string>(nullable: true),
                    Chasquido = table.Column<string>(nullable: true),
                    Crepitacion = table.Column<string>(nullable: true),
                    Cuello = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Grandulas = table.Column<string>(nullable: true),
                    Labios = table.Column<string>(nullable: true),
                    Lengua = table.Column<string>(nullable: true),
                    Paladar_D = table.Column<string>(nullable: true),
                    Paladar_I = table.Column<string>(nullable: true),
                    Piso_Boca = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sesope", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cita",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Activa = table.Column<int>(nullable: false),
                    Asunto = table.Column<string>(nullable: true),
                    ConsultorioId = table.Column<int>(nullable: false),
                    Creacion = table.Column<string>(nullable: true),
                    EspecialistaId = table.Column<int>(nullable: false),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Prioridad = table.Column<int>(nullable: false),
                    Timestamp = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cita", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cita_Consultorio_ConsultorioId",
                        column: x => x.ConsultorioId,
                        principalTable: "Consultorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Especialista",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Apellido = table.Column<string>(nullable: true),
                    ConsultorioId = table.Column<int>(nullable: false),
                    Egresado = table.Column<string>(nullable: true),
                    Especialidad = table.Column<string>(nullable: true),
                    Especialisat = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Nacimiento = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Rfc = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false),
                    Vinculo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Especialista", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Especialista_Consultorio_ConsultorioId",
                        column: x => x.ConsultorioId,
                        principalTable: "Consultorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Apellido = table.Column<string>(nullable: true),
                    ConsultorioId = table.Column<int>(nullable: true),
                    Contra = table.Column<string>(nullable: true),
                    Correo = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    HuellaId = table.Column<int>(nullable: true),
                    Imagen = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    PermisoId = table.Column<int>(nullable: false),
                    Telefono = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true),
                    Usuario = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Usuario_Consultorio_ConsultorioId",
                        column: x => x.ConsultorioId,
                        principalTable: "Consultorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Usuario_Permisos_PermisoId",
                        column: x => x.PermisoId,
                        principalTable: "Permisos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Examen_Dental",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Autorizacion = table.Column<string>(nullable: true),
                    Costo = table.Column<float>(nullable: false),
                    Diagnostico = table.Column<string>(nullable: true),
                    Diente = table.Column<int>(nullable: false),
                    Diferencia = table.Column<float>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Pago = table.Column<float>(nullable: false),
                    Timestamp = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Examen_Dental", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Examen_Dental_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Examen_Facial",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ATM = table.Column<string>(nullable: true),
                    ATM_A_D = table.Column<string>(nullable: true),
                    ATM_A_I = table.Column<string>(nullable: true),
                    ATM_C_D = table.Column<string>(nullable: true),
                    ATM_C_I = table.Column<string>(nullable: true),
                    Amigdalas = table.Column<string>(nullable: true),
                    Cara = table.Column<string>(nullable: true),
                    Carrillos = table.Column<string>(nullable: true),
                    Chasquido = table.Column<string>(nullable: true),
                    Crepitacion = table.Column<string>(nullable: true),
                    Cuello = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Grandulas = table.Column<string>(nullable: true),
                    Labios = table.Column<string>(nullable: true),
                    Lengua = table.Column<string>(nullable: true),
                    Paladar_D = table.Column<string>(nullable: true),
                    Paladar_I = table.Column<string>(nullable: true),
                    Piso_Boca = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Examen_Facial", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Examen_Facial_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Huella",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Creacion = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Huella = table.Column<string>(nullable: true),
                    Modificacion = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Huella", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Huella_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Db_Notas_Paciente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Observaciones = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Db_Notas_Paciente", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Db_Notas_Paciente_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Interrogatorio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ExamenId = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    NumeroPregunta = table.Column<int>(nullable: false),
                    Respuesta = table.Column<int>(nullable: false),
                    Timestamp = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interrogatorio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Interrogatorio_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Db_Paciente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Apellido = table.Column<string>(nullable: true),
                    Direccion = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Estado_civil = table.Column<int>(nullable: false),
                    ExamenId = table.Column<int>(nullable: false),
                    Expediente = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Fecha_Nacimiento = table.Column<string>(nullable: true),
                    HuellaId = table.Column<int>(nullable: false),
                    Imagen = table.Column<string>(nullable: true),
                    Lugar_Nacimiento = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Ocupacion = table.Column<string>(nullable: true),
                    Referencia = table.Column<string>(nullable: true),
                    Relacion = table.Column<string>(nullable: true),
                    Sexo = table.Column<int>(nullable: false),
                    Telefono_Casa = table.Column<string>(nullable: true),
                    Telefono_Celular = table.Column<string>(nullable: true),
                    Telefono_Oficina = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true),
                    Tutor = table.Column<string>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Db_Paciente", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Db_Paciente_Huella_HuellaId",
                        column: x => x.HuellaId,
                        principalTable: "Huella",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Db_Paciente_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Examen",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Db_ExamenDental_Id = table.Column<int>(nullable: true),
                    Db_ExamenFacial_Id = table.Column<int>(nullable: true),
                    Db_Paciente_Id = table.Column<int>(nullable: true),
                    Db_Usuario_Id = table.Column<int>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    Fecha = table.Column<string>(nullable: true),
                    Timestamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Examen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Examen_Examen_Dental_Db_ExamenDental_Id",
                        column: x => x.Db_ExamenDental_Id,
                        principalTable: "Examen_Dental",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Examen_Examen_Facial_Db_ExamenFacial_Id",
                        column: x => x.Db_ExamenFacial_Id,
                        principalTable: "Examen_Facial",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Examen_Db_Paciente_Db_Paciente_Id",
                        column: x => x.Db_Paciente_Id,
                        principalTable: "Db_Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Examen_Usuario_Db_Usuario_Id",
                        column: x => x.Db_Usuario_Id,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cita_ConsultorioId",
                table: "Cita",
                column: "ConsultorioId");

            migrationBuilder.CreateIndex(
                name: "IX_Cita_EspecialistaId",
                table: "Cita",
                column: "EspecialistaId");

            migrationBuilder.CreateIndex(
                name: "IX_Cita_PacienteId",
                table: "Cita",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Cita_UsuarioId",
                table: "Cita",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Especialista_ConsultorioId",
                table: "Especialista",
                column: "ConsultorioId");

            migrationBuilder.CreateIndex(
                name: "IX_Especialista_UsuarioId",
                table: "Especialista",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Examen_Db_ExamenDental_Id",
                table: "Examen",
                column: "Db_ExamenDental_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Examen_Db_ExamenFacial_Id",
                table: "Examen",
                column: "Db_ExamenFacial_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Examen_Db_Paciente_Id",
                table: "Examen",
                column: "Db_Paciente_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Examen_Db_Usuario_Id",
                table: "Examen",
                column: "Db_Usuario_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Examen_Dental_UsuarioId",
                table: "Examen_Dental",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Examen_Facial_UsuarioId",
                table: "Examen_Facial",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Huella_UsuarioId",
                table: "Huella",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Interrogatorio_ExamenId",
                table: "Interrogatorio",
                column: "ExamenId");

            migrationBuilder.CreateIndex(
                name: "IX_Interrogatorio_UsuarioId",
                table: "Interrogatorio",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Db_Notas_Paciente_UsuarioId",
                table: "Db_Notas_Paciente",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Db_Paciente_ExamenId",
                table: "Db_Paciente",
                column: "ExamenId");

            migrationBuilder.CreateIndex(
                name: "IX_Db_Paciente_HuellaId",
                table: "Db_Paciente",
                column: "HuellaId");

            migrationBuilder.CreateIndex(
                name: "IX_Db_Paciente_UsuarioId",
                table: "Db_Paciente",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario_ConsultorioId",
                table: "Usuario",
                column: "ConsultorioId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario_HuellaId",
                table: "Usuario",
                column: "HuellaId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario_PermisoId",
                table: "Usuario",
                column: "PermisoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cita_Especialista_EspecialistaId",
                table: "Cita",
                column: "EspecialistaId",
                principalTable: "Especialista",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cita_Db_Paciente_PacienteId",
                table: "Cita",
                column: "PacienteId",
                principalTable: "Db_Paciente",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cita_Usuario_UsuarioId",
                table: "Cita",
                column: "UsuarioId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Especialista_Usuario_UsuarioId",
                table: "Especialista",
                column: "UsuarioId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuario_Huella_HuellaId",
                table: "Usuario",
                column: "HuellaId",
                principalTable: "Huella",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Interrogatorio_Examen_ExamenId",
                table: "Interrogatorio",
                column: "ExamenId",
                principalTable: "Examen",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Db_Paciente_Examen_ExamenId",
                table: "Db_Paciente",
                column: "ExamenId",
                principalTable: "Examen",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Usuario_Consultorio_ConsultorioId",
                table: "Usuario");

            migrationBuilder.DropForeignKey(
                name: "FK_Examen_Db_Paciente_Db_Paciente_Id",
                table: "Examen");

            migrationBuilder.DropForeignKey(
                name: "FK_Huella_Usuario_UsuarioId",
                table: "Huella");

            migrationBuilder.DropTable(
                name: "Cita");

            migrationBuilder.DropTable(
                name: "Dispositivo");

            migrationBuilder.DropTable(
                name: "Interrogatorio");

            migrationBuilder.DropTable(
                name: "Db_Notas_Paciente");

            migrationBuilder.DropTable(
                name: "Sesope");

            migrationBuilder.DropTable(
                name: "Especialista");

            migrationBuilder.DropTable(
                name: "Consultorio");

            migrationBuilder.DropTable(
                name: "Db_Paciente");

            migrationBuilder.DropTable(
                name: "Examen");

            migrationBuilder.DropTable(
                name: "Examen_Dental");

            migrationBuilder.DropTable(
                name: "Examen_Facial");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Huella");

            migrationBuilder.DropTable(
                name: "Permisos");
        }
    }
}
