﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using OdontologiaDental.Data;

namespace OdontologiaDental.Migrations
{
    [DbContext(typeof(DatosContext))]
    [Migration("20171109042811_Dental")]
    partial class Dental
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.3");

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Cita", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Activa");

                    b.Property<string>("Asunto");

                    b.Property<int>("ConsultorioId");

                    b.Property<string>("Creacion");

                    b.Property<int>("EspecialistaId");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<int>("PacienteId");

                    b.Property<int>("Prioridad");

                    b.Property<string>("Timestamp");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("ConsultorioId");

                    b.HasIndex("EspecialistaId");

                    b.HasIndex("PacienteId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Cita");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Consultorio", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Consultorio");

                    b.Property<string>("Direccion");

                    b.Property<string>("Encargado");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.HasKey("Id");

                    b.ToTable("Consultorio");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Dispositivo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Mac");

                    b.Property<string>("Nombre");

                    b.Property<string>("Serie");

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("Dispositivo");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Especialista", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Apellido");

                    b.Property<int>("ConsultorioId");

                    b.Property<string>("Egresado");

                    b.Property<string>("Especialidad");

                    b.Property<string>("Especialisat");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<string>("Nacimiento");

                    b.Property<string>("Nombre");

                    b.Property<string>("Rfc");

                    b.Property<int>("UsuarioId");

                    b.Property<int>("Vinculo");

                    b.HasKey("Id");

                    b.HasIndex("ConsultorioId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Especialista");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Examen", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("Db_ExamenDental_Id");

                    b.Property<int?>("Db_ExamenFacial_Id");

                    b.Property<int?>("Db_Paciente_Id");

                    b.Property<int?>("Db_Usuario_Id");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<string>("Timestamp");

                    b.HasKey("Id");

                    b.HasIndex("Db_ExamenDental_Id");

                    b.HasIndex("Db_ExamenFacial_Id");

                    b.HasIndex("Db_Paciente_Id");

                    b.HasIndex("Db_Usuario_Id");

                    b.ToTable("Examen");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_ExamenDental", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Autorizacion");

                    b.Property<float>("Costo");

                    b.Property<string>("Diagnostico");

                    b.Property<int>("Diente");

                    b.Property<float>("Diferencia");

                    b.Property<string>("Fecha");

                    b.Property<float>("Pago");

                    b.Property<string>("Timestamp");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Examen_Dental");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_ExamenFacial", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ATM");

                    b.Property<string>("ATM_A_D");

                    b.Property<string>("ATM_A_I");

                    b.Property<string>("ATM_C_D");

                    b.Property<string>("ATM_C_I");

                    b.Property<string>("Amigdalas");

                    b.Property<string>("Cara");

                    b.Property<string>("Carrillos");

                    b.Property<string>("Chasquido");

                    b.Property<string>("Crepitacion");

                    b.Property<string>("Cuello");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<string>("Grandulas");

                    b.Property<string>("Labios");

                    b.Property<string>("Lengua");

                    b.Property<string>("Paladar_D");

                    b.Property<string>("Paladar_I");

                    b.Property<string>("Piso_Boca");

                    b.Property<string>("Timestamp");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Examen_Facial");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Huella", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Creacion");

                    b.Property<int>("Estado");

                    b.Property<string>("Huella");

                    b.Property<string>("Modificacion");

                    b.Property<string>("Timestamp");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Huella");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Interrogatorio", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ExamenId");

                    b.Property<string>("Fecha");

                    b.Property<int>("NumeroPregunta");

                    b.Property<int>("Respuesta");

                    b.Property<string>("Timestamp");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("ExamenId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Interrogatorio");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Notas_Paciente", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<string>("Observaciones");

                    b.Property<string>("Timestamp");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Db_Notas_Paciente");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Paciente", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Apellido");

                    b.Property<string>("Direccion");

                    b.Property<int>("Estado");

                    b.Property<int>("Estado_civil");

                    b.Property<int>("ExamenId");

                    b.Property<int>("Expediente");

                    b.Property<string>("Fecha");

                    b.Property<string>("Fecha_Nacimiento");

                    b.Property<int>("HuellaId");

                    b.Property<string>("Imagen");

                    b.Property<string>("Lugar_Nacimiento");

                    b.Property<string>("Nombre");

                    b.Property<string>("Ocupacion");

                    b.Property<string>("Referencia");

                    b.Property<string>("Relacion");

                    b.Property<int>("Sexo");

                    b.Property<string>("Telefono_Casa");

                    b.Property<string>("Telefono_Celular");

                    b.Property<string>("Telefono_Oficina");

                    b.Property<string>("Timestamp");

                    b.Property<string>("Tutor");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("ExamenId");

                    b.HasIndex("HuellaId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Db_Paciente");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Permisos", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Estado");

                    b.Property<string>("Nombre");

                    b.HasKey("Id");

                    b.ToTable("Permisos");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Sesope", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ATM");

                    b.Property<string>("ATM_A_D");

                    b.Property<string>("ATM_A_I");

                    b.Property<string>("ATM_C_D");

                    b.Property<string>("ATM_C_I");

                    b.Property<string>("Amigdalas");

                    b.Property<string>("Cara");

                    b.Property<string>("Carrillos");

                    b.Property<string>("Chasquido");

                    b.Property<string>("Crepitacion");

                    b.Property<string>("Cuello");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<string>("Grandulas");

                    b.Property<string>("Labios");

                    b.Property<string>("Lengua");

                    b.Property<string>("Paladar_D");

                    b.Property<string>("Paladar_I");

                    b.Property<string>("Piso_Boca");

                    b.Property<string>("Timestamp");

                    b.HasKey("Id");

                    b.ToTable("Sesope");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Usuario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Apellido");

                    b.Property<int?>("ConsultorioId");

                    b.Property<string>("Contra");

                    b.Property<string>("Correo");

                    b.Property<int>("Estado");

                    b.Property<string>("Fecha");

                    b.Property<int?>("HuellaId");

                    b.Property<string>("Imagen");

                    b.Property<string>("Nombre");

                    b.Property<int>("PermisoId");

                    b.Property<string>("Telefono");

                    b.Property<string>("Timestamp");

                    b.Property<string>("Usuario");

                    b.HasKey("Id");

                    b.HasIndex("ConsultorioId");

                    b.HasIndex("HuellaId");

                    b.HasIndex("PermisoId");

                    b.ToTable("Usuario");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Cita", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Consultorio", "Consultorio")
                        .WithMany()
                        .HasForeignKey("ConsultorioId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Especialista", "Especialista")
                        .WithMany()
                        .HasForeignKey("EspecialistaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Paciente", "Paciente")
                        .WithMany()
                        .HasForeignKey("PacienteId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Especialista", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Consultorio", "Consultorio")
                        .WithMany()
                        .HasForeignKey("ConsultorioId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Examen", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_ExamenDental", "Examen_Dental")
                        .WithMany()
                        .HasForeignKey("Db_ExamenDental_Id");

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_ExamenFacial", "Examen_Facial")
                        .WithMany()
                        .HasForeignKey("Db_ExamenFacial_Id");

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Paciente", "Paciente")
                        .WithMany()
                        .HasForeignKey("Db_Paciente_Id");

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("Db_Usuario_Id");
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_ExamenDental", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_ExamenFacial", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Huella", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Interrogatorio", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Examen", "Examen")
                        .WithMany("Interrogatorio")
                        .HasForeignKey("ExamenId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Notas_Paciente", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Paciente", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Examen", "Examen")
                        .WithMany()
                        .HasForeignKey("ExamenId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Huella", "Huella")
                        .WithMany()
                        .HasForeignKey("HuellaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OdontologiaDental.Data.TypesTablas.Db_Usuario", b =>
                {
                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Consultorio", "Consultorio")
                        .WithMany()
                        .HasForeignKey("ConsultorioId");

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Huella", "Huella")
                        .WithMany()
                        .HasForeignKey("HuellaId");

                    b.HasOne("OdontologiaDental.Data.TypesTablas.Db_Permisos", "Permiso")
                        .WithMany()
                        .HasForeignKey("PermisoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
