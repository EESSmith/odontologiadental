﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Usuario
    {
        
        public int Id { get; set; }        
        public int Estado { get; set; }
        //[KeyAttribute]
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Contra { get; set; }
        public string Correo { get; set; }
        public int PermisoId { get; set; }
        public int? HuellaId { get; set; }
        public int? ConsultorioId { get; set; }
        public string Fecha { get; set; }
        public string Imagen { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }

        #region Relaciones
        [ForeignKey("PermisoId")]
        public Db_Permisos Permiso { get; set; }
        [ForeignKey("HuellaId")]
        public Db_Huella Huella { get; set; }
        [ForeignKey("ConsultorioId")]
        public Db_Consultorio Consultorio { get; set; }
        #endregion


        //public static explicit operator DispositivoType(Dispositivo source)
        //{
        //    return new DispositivoType()
        //    {
        //        Id = source.Id,
        //        CadenaInicial = source.CadenaInicial,
        //        CadenaFinal = source.CadenaFinal,
        //        Descripcion = source.Descripcion,
        //        EnviaRepetidos = source.EnviaRepetidos,
        //        MaximoPermitido = source.MaximoPermitido,
        //        MinimoPermitido = source.MinimoPermitido,
        //        Movil = source.Movil,
        //        Multiplicador = source.Multiplicador,
        //        PasaProCero = source.PasaProCero,
        //        UnDatoDeCuantos = source.UnDatoDeCuantos,
        //        TipoConexion = source.TipoConexion,
        //    };
        //}
    }
}
