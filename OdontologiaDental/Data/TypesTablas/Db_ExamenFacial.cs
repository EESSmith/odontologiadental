﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_ExamenFacial
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Fecha { get; set; }
        public string Cara { get; set; }
        public string Labios { get; set; }
        public string Carrillos { get; set; }
        public string Grandulas { get; set; }
        public string Paladar_D { get; set; }
        public string Paladar_I { get; set; }
        public string Lengua { get; set; }
        public string Piso_Boca { get; set; }
        public string Amigdalas { get; set; }
        public string Cuello { get; set; }
        public string ATM { get; set; }
        public string Chasquido { get; set; }
        public string Crepitacion { get; set; }
        public string ATM_A_D { get; set; }
        public string ATM_A_I { get; set; }
        public string ATM_C_D { get; set; }
        public string ATM_C_I { get; set; }

        public int UsuarioId { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }
        #region Relaciones
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        #endregion
    }

}
