﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Interrogatorio
    {
        public int Id { get; set; }
        public string Fecha { get; set; }
        public int ExamenId { get; set; }
        public int NumeroPregunta { get; set; }
        public int Respuesta { get; set; }
        public int UsuarioId { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }
        #region Relaciones
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        [ForeignKey("ExamenId")]
        public Db_Examen Examen { get; set; }
        #endregion

    }
}
