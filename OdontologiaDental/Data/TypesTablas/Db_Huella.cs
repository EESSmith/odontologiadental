﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Huella
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Huella { get; set; }
        public string Creacion { get; set; }
        public string Modificacion { get; set; }
        public int UsuarioId { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }
        #region Relaciones
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        #endregion
    }
}
