﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Consultorio
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Consultorio { get; set; }
        public string Fecha { get; set; }
        public string Direccion { get; set; }
        public string Encargado { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public override string ToString()
        {
            return this.Consultorio;
        }
    }
}
