﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Examen
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Fecha { get; set; }
        [ForeignKey("Db_Usuario_Id")]
        public Db_Usuario Usuario { get; set; }
        [ForeignKey("Db_Paciente_Id")]
        public Db_Paciente Paciente { get; set; }
        [ForeignKey("Db_ExamenFacial_Id")]
        public Db_ExamenFacial Examen_Facial { get; set; }
        [ForeignKey("Db_ExamenDental_Id")]
        public Db_ExamenDental Examen_Dental { get; set; }
        public List<Db_Interrogatorio> Interrogatorio { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }

    }
}
