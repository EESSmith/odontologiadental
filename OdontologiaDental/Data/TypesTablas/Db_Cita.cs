﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Cita
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Creacion { get; set; }
        public string Fecha { get; set; }
        public string Asunto { get; set; }
        public int Prioridad { get; set; }
        public int Activa { get; set; }
        public int ConsultorioId { get; set; }
        public int EspecialistaId { get; set; }
        public int UsuarioId { get; set; }
        public int PacienteId { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }

        #region Relaciones
        [ForeignKey("ConsultorioId")]
        public Db_Consultorio Consultorio { get; set; }
        [ForeignKey("EspecialistaId")]
        public Db_Especialista Especialista { get; set; }
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        [ForeignKey("PacienteId")]
        public Db_Paciente Paciente { get; set; }
        #endregion

    }


}
