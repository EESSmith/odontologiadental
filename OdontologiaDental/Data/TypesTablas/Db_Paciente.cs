﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Paciente
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public int Expediente { get; set; }
        public string Fecha { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Sexo { get; set; }
        public string Lugar_Nacimiento { get; set; }
        public string Fecha_Nacimiento { get; set; }
        public string Direccion { get; set; }
        public string Ocupacion { get; set; }
        public int Estado_civil { get; set; }
        public string Telefono_Oficina { get; set; }
        public string Telefono_Casa { get; set; }
        public string Telefono_Celular { get; set; }
        public string Referencia { get; set; }
        public string Relacion { get; set; }
        public string Tutor { get; set; }
        public int UsuarioId { get; set; }
        public int HuellaId { get; set; }
        public string Imagen { get; set; }
        public int ExamenId { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }

        #region Relaciones
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        [ForeignKey("HuellaId")]
        public Db_Huella Huella { get; set; }
        [ForeignKey("ExamenId")]
        public Db_Examen Examen { get; set; }
        #endregion


    }
}
