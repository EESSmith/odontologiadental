﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Permisos
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Nombre { get; set; }

    }
}
