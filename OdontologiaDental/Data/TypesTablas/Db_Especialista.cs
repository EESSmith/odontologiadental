﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_Especialista
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Especialisat { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Vinculo { get; set; }
        public string Especialidad { get; set; }
        public string Fecha { get; set; }
        public int UsuarioId { get; set; }
        public string Egresado { get; set; }
        public string Nacimiento { get; set; }
        public string Rfc { get; set; }
        public int ConsultorioId { get; set; }

        #region Relaciones
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        [ForeignKey("ConsultorioId")]
        public Db_Consultorio Consultorio { get; set; }
        #endregion
    }
}
