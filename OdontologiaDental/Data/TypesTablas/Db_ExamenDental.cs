﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data.TypesTablas
{
    public class Db_ExamenDental
    {
        public int Id { get; set; }
        public string Fecha { get; set; }
        public int Diente { get; set; }
        public string Diagnostico { get; set; }
        public float Costo { get; set; }
        public float Pago { get; set; }
        public float Diferencia { get; set; }
        public string Autorizacion { get; set; }
        public int UsuarioId { get; set; }
        //[Timestamp]
        public string Timestamp { get; set; }

        #region Relaciones
        [ForeignKey("UsuarioId")]
        public Db_Usuario Usuario { get; set; }
        #endregion
    }
}
