﻿using Microsoft.EntityFrameworkCore;
using OdontologiaDental.Data.TypesTablas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OdontologiaDental.Data
{

    //Add-Migration
    //Remove-Migration
    public class DatosContext : DbContext
    {

        public DbSet<Db_Cita> Cita { get; set; }
        public DbSet<Db_Consultorio> Consultorio { get; set; }
        public DbSet<Db_Dispositivo> Dispositivo { get; set; }
        public DbSet<Db_Especialista> Especialista { get; set; }
        public DbSet<Db_Examen> Examen { get; set; }
        public DbSet<Db_ExamenDental> Examen_Dental { get; set; }
        public DbSet<Db_ExamenFacial> Examen_Facial { get; set; }
        public DbSet<Db_Huella> Huella { get; set; }
        public DbSet<Db_Interrogatorio> Interrogatorio { get; set; }
        public DbSet<Db_Notas_Paciente> Notas_paciente { get; set; }
        public DbSet<Db_Notas_Paciente> Paciente { get; set; }
        public DbSet<Db_Permisos> Permisos { get; set; }
        public DbSet<Db_Sesope> Sesope { get; set; }
        public DbSet<Db_Usuario> Usuario{ get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=Dental.db");
        }
        async void GuardarDatos(DbContextOptionsBuilder optionsBuilder)
             {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values["exampleSetting"] = "Hello Windows";
            var storageFolder2 = KnownFolders.MusicLibrary;
            var storageFolder = KnownFolders.DocumentsLibrary;
            var var = storageFolder2.Path;
            optionsBuilder.UseSqlite("Filename="+storageFolder.Path+"Dental.db");
            //var file = await storageFolder.CreateFileAsync(Path.ChangeExtension(fileName, ".cal"), CreationCollisionOption.ReplaceExisting);
            //await FileIO.WriteTextAsync(file, String.Join(",", lines));

            //localFolder = await localFolder.CreateFolderAsync("Datos");

            StorageFolder localFolder = ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = await localFolder.CreateFileAsync("dataFile_Dental.txt", CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(sampleFile, "Hola");
            
        }
    }

}
