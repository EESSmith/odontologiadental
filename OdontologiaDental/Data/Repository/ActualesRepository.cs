﻿using OdontologiaDental.Types;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OdontologiaDental.Data.TypesTablas;
using OdontologiaDental.Design;

namespace OdontologiaDental.Data
{
    class  ActualesRepository : IActualesRepository

    {
        #region "Variables"
        private ObservableCollection<Db_Usuario> _usuario ;
        private DateTime _hora;

        #endregion
        #region "Eventos"
        #endregion
        #region "Constructor"
        public ActualesRepository()
        {
            if (DameUsuarios().Count==0)
            {
                InsertaDatosPredeterminados();
            }
                      

        }
        #endregion
        #region "Interno"
        public ObservableCollection<Db_Usuario> DameUsuarios()
        {
            try
            {               
                using (var db = new DatosContext())
                {                    
                    ObservableCollection<Db_Usuario> a = new ObservableCollection<Db_Usuario>(db.Usuario.Include(f=>f.Permiso).Include(h=>h.Consultorio).Include(g=>g.Huella).Select(p => p).Where(u=>u.Estado==1));
                    var user = db.Usuario;
                    return a;
                }
            }
            catch (Exception ex)
            {
                return new ObservableCollection<Db_Usuario>();
            }
        }
        public ObservableCollection<Db_Permisos> DamePermisos()
        {
            try
            {
                using (var db = new DatosContext())
                {
                    ObservableCollection<Db_Permisos> a = new ObservableCollection<Db_Permisos>(db.Permisos.Select(p => p).Where(f=>f.Estado==1));
                    var user = db.Usuario;
                    return a;
                }
            }
            catch (Exception ex)
            {
                return new ObservableCollection<Db_Permisos>();
            }
        }
        public ObservableCollection<Db_Consultorio> DameListaConsultorios()
        {
            try
            {
                using (var db = new DatosContext())
                {
                    ObservableCollection<Db_Consultorio> a = new ObservableCollection<Db_Consultorio>(db.Consultorio.Select(p => p).Where(j => j.Estado==1));
                    return a;
                }
            }
            catch (Exception ex)
            {
                return new ObservableCollection<Db_Consultorio>();
            }
        }
        public bool InsertaDatosPredeterminados()
        {
            try
            {
                using (var db = new DatosContext())
                {
                    var tiempo = DameHora().ToString("yyyy/MM/dd HH:mm:ss");
                    db.Permisos.Add( new Db_Permisos { Estado=1,Id=1,Nombre= "Administrador" });
                    db.Permisos.Add(new Db_Permisos { Estado = 1, Id = 2, Nombre = "Asistente" });
                    db.Permisos.Add(new Db_Permisos { Estado = 1, Id = 3, Nombre = "Doctor" });
                    db.SaveChanges();
                    var permiso = db.Permisos.FirstOrDefault();
                    db.Usuario.Add(new Db_Usuario {Usuario="admin",Contra="admin",Nombre="User",Permiso= permiso, Fecha = tiempo, Timestamp = tiempo, Estado =1 } );
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DateTime DameHora()
        {
            return DateTime.Now;

        }
        public Db_Usuario InicioUsuario(string usuario, string password)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    var _usuario = db.Usuario.AsEnumerable().Select(Q => Q).Where( p=> p.Usuario.ToUpper() == usuario.ToUpper() && p.Contra == password &&p.Estado==1).FirstOrDefault();

                    if (_usuario!=null)
                    {
                        Db_Usuario accesoUser = db.Usuario.Where(p=>p.Id==_usuario.Id).FirstOrDefault();
                        accesoUser.Timestamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        
                        db.SaveChanges();
                        _usuario = accesoUser;
                        return _usuario;
                    }
                    return new Db_Usuario();
                }
            }
            catch (Exception ex)
            {
                return new Db_Usuario(); 
            }
        }

        public bool GuardarNuevoUsuario(Db_Usuario Usuario)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    if (Usuario!=null)
                    {
                        
                        Db_Usuario usuarioMod = new Db_Usuario();
                        usuarioMod.Nombre = Usuario.Nombre;
                        usuarioMod.Apellido = Usuario.Apellido;
                        usuarioMod.ConsultorioId = Usuario.Consultorio.Id;
                        usuarioMod.Contra = Usuario.Contra;
                        usuarioMod.Correo = Usuario.Correo;
                        usuarioMod.HuellaId = Usuario.HuellaId;
                        usuarioMod.Telefono = Usuario.Telefono;
                        usuarioMod.HuellaId = Usuario.HuellaId;
                        usuarioMod.PermisoId = Usuario.PermisoId;
                        usuarioMod.Usuario = Usuario.Usuario;
                        usuarioMod.Timestamp = DameHora().ToString("yyyy/MM/dd HH:mm:ss");
                        usuarioMod.Fecha = DameHora().ToString("yyyy/MM/dd HH:mm:ss");
                        usuarioMod.Estado = 1;

                        db.Usuario.Add(usuarioMod);
                        db.SaveChanges();
                        return true;
                    }
                    else return false;                   
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool GuardarUsuario(Db_Usuario Usuario)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    if (Usuario != null)
                    {
                        var usuarioMod = db.Usuario.Include(f => f.Permiso).Include(h => h.Consultorio).Include(g => g.Huella).Where(p=>p.Id== Usuario.Id).FirstOrDefault();
                        usuarioMod.Nombre = Usuario.Nombre;
                        usuarioMod.Apellido = Usuario.Apellido;
                        usuarioMod.ConsultorioId = Usuario.ConsultorioId;
                        usuarioMod.Contra = Usuario.Contra;
                        usuarioMod.Correo = Usuario.Correo;
                        usuarioMod.HuellaId = Usuario.HuellaId;
                        usuarioMod.Telefono = Usuario.Telefono;
                        usuarioMod.HuellaId = Usuario.HuellaId;
                        usuarioMod.PermisoId = Usuario.PermisoId;
                        usuarioMod.Usuario = Usuario.Usuario;
                        usuarioMod.Timestamp= DameHora().ToString("yyyy/MM/dd HH:mm:ss");
                        db.SaveChanges();
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool BorrarUsuario(Db_Usuario Usuario)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    if (Usuario != null)
                    {
                        var usuarioMod = db.Usuario.Where(p => p.Id == Usuario.Id).FirstOrDefault();
                        usuarioMod.Estado = 0;
                        db.SaveChanges();
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool GuardarNuevoConsultorio(Db_Consultorio Consultorio)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    if (Consultorio != null)
                    {
                        Db_Consultorio usuarioMod = new Db_Consultorio();
                        usuarioMod.Latitude = Consultorio.Latitude;
                        usuarioMod.Longitude = Consultorio.Longitude;
                        usuarioMod.Direccion = Consultorio.Direccion;
                        usuarioMod.Encargado = Consultorio.Encargado;
                        usuarioMod.Consultorio = Consultorio.Consultorio;
                        usuarioMod.Fecha = DameHora().ToString("yyyy/MM/dd HH:mm:ss");
                        usuarioMod.Estado = 1;
                        db.Consultorio.Add(usuarioMod);
                        db.SaveChanges();
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool GuardarConsultorio(Db_Consultorio Consultorio)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    if (Consultorio != null)
                    {
                        Db_Consultorio usuarioMod = db.Consultorio.Where(p => p.Id == Consultorio.Id).FirstOrDefault();
                        usuarioMod.Latitude = Consultorio.Latitude;
                        usuarioMod.Longitude = Consultorio.Longitude;
                        usuarioMod.Direccion = Consultorio.Direccion;
                        usuarioMod.Encargado = Consultorio.Encargado;
                        usuarioMod.Consultorio = Consultorio.Consultorio;
                        usuarioMod.Fecha = DameHora().ToString("yyyy/MM/dd HH:mm:ss");
                        db.SaveChanges();
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool BorrarConsultorio(Db_Consultorio Consultorio)
        {
            try
            {
                using (var db = new DatosContext())
                {
                    if (Consultorio != null)
                    {
                        var usuarioMod = db.Consultorio.Where(p => p.Id == Consultorio.Id).FirstOrDefault();
                        usuarioMod.Estado = 0;
                        db.SaveChanges();
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }




        #endregion
        #region "Externo"
        #endregion



    }
}
