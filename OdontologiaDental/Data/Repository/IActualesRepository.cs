﻿using OdontologiaDental.Data.TypesTablas;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Data
{
    #region "Variables"
    #endregion
    #region "Constructor"
    #endregion
    public interface IActualesRepository
    {
        #region "Interno"
        Db_Usuario InicioUsuario(string usuario, string password);
        ObservableCollection<Db_Usuario> DameUsuarios();
        ObservableCollection<Db_Permisos> DamePermisos();
        ObservableCollection<Db_Consultorio> DameListaConsultorios();
        bool GuardarUsuario(Db_Usuario Usuario);
        bool GuardarNuevoUsuario(Db_Usuario Usuario);
        bool BorrarUsuario(Db_Usuario Usuario);

        bool GuardarConsultorio(Db_Consultorio Consultorio);
        bool GuardarNuevoConsultorio(Db_Consultorio Consultorio);
        bool BorrarConsultorio(Db_Consultorio Consultorio);


        #endregion
        #region "Externo"
        #endregion









    }

}
