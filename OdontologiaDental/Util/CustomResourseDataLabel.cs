﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Resources;

namespace OdontologiaDental.Util
{
    public class CustomResourseDataLabel : CustomXamlResourceLoader
    {
        private string LanguageLabel;
        private ResourceLoader keyValues;

        public CustomResourseDataLabel(string data="")
        {
            this.LanguageLabel = data;
            if (data!=string.Empty)
            {
                ResourceContext.SetGlobalQualifierValue("Language", data);

            }
            this.keyValues = ResourceLoader.GetForCurrentView();
        }



        protected override object GetResource(string resourceId, string objectType, string propertyName, string propertyType)
        {
            var data = this.keyValues.GetString(resourceId);

            if (data==string.Empty)
            {
                return "-invalid data-";
            }
            else
            {
                return data;
            }
        }
    }
}
