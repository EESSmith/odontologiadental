﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using OdontologiaDental.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OdontologiaDental.ViewModel
{
    public class PacienteViewModel : ViewModelBase
    {

        private IBusinessLayer _BusinessLayer;

        private ICommand pageLoaded;
        public ICommand PageLoadedCommand
        {
            get
            {
                if (pageLoaded == null)
                {
                    pageLoaded = new RelayCommand(PageLoaded);
                }
                return pageLoaded;
            }
        }

        public PacienteViewModel(IBusinessLayer businessLayer)
        {
            _BusinessLayer = businessLayer;
        }
        public void PageLoaded()
        {
          
        }


    }
}
