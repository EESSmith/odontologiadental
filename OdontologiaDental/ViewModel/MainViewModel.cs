﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using OdontologiaDental.Model;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media;
using Windows.UI;

namespace OdontologiaDental.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IBusinessLayer _BusinessLayer;
        private readonly INavigationService _navigationService;
        private string _clock = "Starting...";
        private int _counter;
        private RelayCommand _incrementCommand;
        private RelayCommand<string> _navigateCommand;
        private bool _runClock;
        private RelayCommand _sendMessageCommand;
        private RelayCommand _showDialogCommand;
        private string _welcomeTitle = string.Empty;


        public SolidColorBrush _margenUsuario;
        public SolidColorBrush _margenContra;        

        private string _usuarioInicio = string.Empty;
        private string _contraInicio = string.Empty;

        public string Clock
        {
            get
            {
                return _clock;
            }
            set
            {
                Set(ref _clock, value);
            }
        }

        public RelayCommand IncrementCommand
        {
            get
            {
                return _incrementCommand
                    ?? (_incrementCommand = new RelayCommand(
                    () =>
                    {
                        WelcomeTitle = string.Format("Counter clicked {0} times", ++_counter);
                    }));
            }
        }
        public RelayCommand<string> NavigateCommand
        {
            get
            {
                return _navigateCommand
                       ?? (_navigateCommand = new RelayCommand<string>(
                           p => _navigationService.NavigateTo(ViewModelLocator.SecondPageKey, p),
                           p => !string.IsNullOrEmpty(p)));
            }
        }
        public RelayCommand SendMessageCommand
        {
            get
            {
                return _sendMessageCommand
                    ?? (_sendMessageCommand = new RelayCommand(
                    () =>
                    {
                        Messenger.Default.Send(
                            new NotificationMessageAction<string>(
                                "Testing",
                                reply =>
                                {
                                    WelcomeTitle = reply;
                                }));
                    }));
            }
        }
        public RelayCommand ShowDialogCommand
        {
            get
            {
                return _showDialogCommand
                       ?? (_showDialogCommand = new RelayCommand(
                           async () =>
                           {
                               var dialog = ServiceLocator.Current.GetInstance<IDialogService>();
                               await dialog.ShowMessage("Hello Universal Application", "it works...");
                           }));
            }
        }
        public RelayCommand IniciarSesion
        {
            get
            {
                return _showDialogCommand
                       ?? (_showDialogCommand = new RelayCommand(
                           async () =>
                           {
                               VerificarSesion();
                           }));
            }
        }

        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                Set(ref _welcomeTitle, value);
            }
        }


        public string UsuarioInicio
        {
            get
            {
                return _usuarioInicio;
            }

            set
            {
                Set(ref _usuarioInicio, value);
            }
        }
        public string ContraInicio
        {
            get
            {
                return _contraInicio;
            }

            set
            {
                Set(ref _contraInicio, value);
            }
        }

        public SolidColorBrush MargenUsuario
        {
            get { return _margenUsuario; }
            set
            {
                if (value != _margenUsuario)
                {
                    _margenUsuario = value;
                    RaisePropertyChanged(() => MargenUsuario);
                }
            }
        }
        public SolidColorBrush MargenContra
        {
            get { return _margenContra; }
            set
            {
                if (value != _margenContra)
                {
                    _margenContra = value;
                    RaisePropertyChanged(() => MargenContra);
                }
            }
        }
        public MainViewModel(IBusinessLayer BusinessLayer,INavigationService navigationService)
        {
            _BusinessLayer = BusinessLayer;
            _navigationService = navigationService;
            //Initialize();
            
        }

        public void RunClock()
        {
            _runClock = true;

            Task.Run(async () =>
            {
                while (_runClock)
                {
                    try
                    {
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            Clock = DateTime.Now.ToString("HH:mm:ss");
                        });

                        await Task.Delay(1000);
                    }
                    catch (Exception)
                    {
                    }
                }
            });
        }

        public void StopClock()
        {
            _runClock = false;
        }

        private  void VerificarSesion()
        {
            if (_usuarioInicio!="")
            {
                MargenUsuario = new SolidColorBrush(Color.FromArgb(255, 20, 20, 20));
                if (_contraInicio!="")
                {
                    MargenUsuario = new SolidColorBrush(Color.FromArgb(255, 20, 20, 20));
                    var respuesta =_BusinessLayer.IniciarSesion(UsuarioInicio, ContraInicio);
                    if (!respuesta)
                    {
                        MargenUsuario = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                        MargenContra = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                    }
                    else
                    {
                        MargenUsuario = new SolidColorBrush(Color.FromArgb(255, 20, 20, 20));
                        MargenContra = new SolidColorBrush(Color.FromArgb(255, 20,20, 20));
                        UsuarioInicio = "";
                        ContraInicio = "";
                    }
                }
                else
                {
                    MargenContra = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
            }
            else
            {
                MargenUsuario = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                if (_contraInicio == "")
                {
                    MargenContra = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
            }
        }

        private async Task Initialize()
        {
            try
            {
                var item = await _BusinessLayer.GetData();
                WelcomeTitle = item.Title;
            }
            catch (Exception ex)
            {
                // Report error here
                WelcomeTitle = ex.Message;
            }
        }
    }
}

//MessageDialog dialog = new MessageDialog(
//                            "La contraseña o el usuario es incorrecto, Intente de nuevo.", "Algo esta mal, por favor intente de nuevo.");
//dialog.Commands.Add(new UICommand("Aceptar"));
//                        IUICommand command = await dialog.ShowAsync();
