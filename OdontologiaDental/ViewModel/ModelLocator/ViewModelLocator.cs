﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using OdontologiaDental.Data;
using OdontologiaDental.Model;
using OdontologiaDental.View;

namespace OdontologiaDental.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        public const string SecondPageKey = "SecondPage";

        /// <summary>
        /// This property can be used to force the application to run with design time data.
        /// </summary>
        public static bool UseDesignTimeData
        {
            get
            {
                return false;
            }
        }

        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            var nav = new NavigationService();
            nav.Configure(typeof(Principal).Name, typeof(Principal));
            nav.Configure(SecondPageKey, typeof(SecondPage));

            SimpleIoc.Default.Register<INavigationService>(() => nav);
            SimpleIoc.Default.Register<IDialogService, DialogService>();
            SimpleIoc.Default.Register<IActualesRepository, ActualesRepository>();

            if (ViewModelBase.IsInDesignModeStatic|| UseDesignTimeData)
            {
                SimpleIoc.Default.Register<IBusinessLayer, Design.DesignBusinessLayer>();
            }
            else
            {
                SimpleIoc.Default.Register<IBusinessLayer, BusinessLayer>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<PrincipalViewModel>();
            SimpleIoc.Default.Register<UsuariosViewModel>();
            SimpleIoc.Default.Register<ConsultorioViewModel>();
            SimpleIoc.Default.Register<PacienteViewModel>();
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
        //    "CA1822:MarkMembersAsStatic",
        //    Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public PrincipalViewModel MainPrincipal => ServiceLocator.Current.GetInstance<PrincipalViewModel>();
        public UsuariosViewModel MainUsuarios => ServiceLocator.Current.GetInstance<UsuariosViewModel>();
        public ConsultorioViewModel MainConsultorio => ServiceLocator.Current.GetInstance<ConsultorioViewModel>();
        public PacienteViewModel MainPaciente => ServiceLocator.Current.GetInstance<PacienteViewModel>();

    }
}
