﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using OdontologiaDental.Model;
using OdontologiaDental.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;

namespace OdontologiaDental.ViewModel
{
    public class PrincipalViewModel : ViewModelBase
    {

        #region "Variables"
        IBusinessLayer _BusinessLayer;
        private ICommand loadedCommand;
        private ICommand cerrar;
        private ICommand abrirMenuCommand;
        private ICommand listBoxSelectionChanged;
        private Page _currentPage;

        private int _indexMenu;
        private object _elementoMenu;


        private bool _abrirmenu;
        #endregion
        #region "Comandos"

        public ICommand C_LoadedCommand
        {
            get
            {
                if (loadedCommand == null)
                {
                    //loadedCommand = new RelayCommand<object>(Navegar);
                }
                return loadedCommand;
            }
        }
        public ICommand CerrarCommand
        {
            get
            {
                if (cerrar == null)
                {
                    cerrar = new RelayCommand<object>(CerrarSesion);
                }
                return cerrar;
            }
        }

        public ICommand AbrirMenuCommand
        {
            get
            {
                if (listBoxSelectionChanged == null)
                {
                    listBoxSelectionChanged = new RelayCommand<object>(AbrirHambuger);
                }
                return listBoxSelectionChanged;
            }
        }
        public ICommand ListBoxSelectionChangedCommand
        {
            get
            {
                if (abrirMenuCommand == null)
                {
                    abrirMenuCommand = new RelayCommand<object>(MenuOpcion);
                }
                return abrirMenuCommand;
            }
        } 

        public Page CurrentPage
        {
            get { return _currentPage; }
            set
            {
                Set(ref this._currentPage, value);
            }
        }
        public int IndexMenu
        {
            get { return _indexMenu; }
            set
            {
                if (value != _indexMenu)
                {
                    _indexMenu = value;
                    RaisePropertyChanged(() => IndexMenu);
                }
            
            }
        }
        public object ElementoMenu
        {
            get { return _elementoMenu; }
            set
            {
                if (value != _elementoMenu)
                {
                    _elementoMenu = value;
                    MenuOpcion(_elementoMenu);
                    RaisePropertyChanged(() => ElementoMenu);
                }

            }
        }

        public bool AbrirMenu
        {
            get { return _abrirmenu; }
            set
            {
                if (value != _abrirmenu)
                {
                    _abrirmenu = value;
                    RaisePropertyChanged(() => AbrirMenu);
                }
            }
        }
        #endregion
        #region "Constructor"
        public PrincipalViewModel(IBusinessLayer BusinessLayer)
        {
            _BusinessLayer = BusinessLayer;
            //Initialize();
           
        }
        #endregion
        #region "Interno"





        #endregion
        #region "Externo"
        private async void CerrarSesion(object pantallaDestino)
        {

            MessageDialog dialog = new MessageDialog(
                           "Todos los datos no guardados se perderan, desea continuar?", "Esta apunto de Cerrar Sesión");
            dialog.Commands.Add(new UICommand("Continuar"));
            dialog.Commands.Add(new UICommand("Cancelar"));
            IUICommand command = await dialog.ShowAsync();            
            if (command.Label.Equals("Continuar", StringComparison.CurrentCultureIgnoreCase))
            {
                _BusinessLayer.CerrarSesion(true);
            }
        }
        private  void AbrirHambuger(object pantallaDestino)
        {
            if (AbrirMenu == true)
            {
                AbrirMenu = false;
            }
            else AbrirMenu = true;
        }
        private void MenuOpcion(object pantallaDestino)
        {
            if (pantallaDestino != null)
            {
                ListBoxItem opcion = (ListBoxItem)pantallaDestino;
                StackPanel dato = (StackPanel)opcion.Content;
                var op = dato.Children;
                TextBlock encabezado = (TextBlock)op[1];

                if (encabezado.Text== "Inicio")
                {

                }else if (encabezado.Text == "Pacientes")
                {
                    CurrentPage = new PacienteView();
                }
                else if (encabezado.Text == "Citas")
                {

                }
                else if (encabezado.Text == "Huella")
                {

                }
                else if (encabezado.Text == "Consultorios")
                {
                    CurrentPage = new Consultorio();
                }
                else if (encabezado.Text == "Usuarios")
                {
                    CurrentPage = new Usuarios();
                }
            }
            
        }


        #endregion





    }
}
