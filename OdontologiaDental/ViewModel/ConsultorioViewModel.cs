﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using OdontologiaDental.Data.TypesTablas;
using OdontologiaDental.Model;
using OdontologiaDental.Types;
using OdontologiaDental.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Resources;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Maps;

namespace OdontologiaDental.ViewModel
{
    public class ConsultorioViewModel : ViewModelBase
    {
        IBusinessLayer _BusinessLayer;
        private ICommand pageLoaded;

        private ICommand guardarCommand;
        private ICommand nuevoCommand;
        private ICommand modificarCommand;
        private ICommand borrarCommand;
        private ICommand cargarMapaCommand;

        private string _mapServiceToken;
        private Geopoint _geoUbicacion;
        private int _zoom;
        private MapStyle _styleMap;
        private string _nombreConsultorio;
        private string _nombre;
        private string _direccion;
        private string _encargado;

        private bool _editarTexto;
        private bool _estadoNuevo;
        private bool _activoNuevo;

        private ObservableCollection<Db_Consultorio> _listaConsultorio;
        private Db_Consultorio _consultorio;

        public static event EventHandler<Ubicacion> RecargarPunto;


        public Geopoint SeattleGeopoint = new Geopoint(new BasicGeoposition() { Latitude = 47.604, Longitude = -122.329 });
        RandomAccessStreamReference mapIconStreamReference;


        public ICommand PageLoadedCommand
        {
            get
            {
                if (pageLoaded == null)
                {
                    pageLoaded = new RelayCommand(PageLoaded);
                }
                return pageLoaded;
            }
        }
        public ICommand GuardarCommand
        {
            get
            {
                if (guardarCommand == null)
                {
                    guardarCommand = new RelayCommand(Guardar);
                }
                return guardarCommand;
            }
        }
        public ICommand NuevoCommand
        {
            get
            {
                if (nuevoCommand == null)
                {
                    nuevoCommand = new RelayCommand(Nuevo);
                }
                return nuevoCommand;
            }
        }
        public ICommand ModificarCommand
        {
            get
            {
                if (modificarCommand == null)
                {
                    modificarCommand = new RelayCommand(Modificar);
                }
                return modificarCommand;
            }
        }
        public ICommand BorrarCommand
        {
            get
            {
                if (borrarCommand == null)
                {
                    borrarCommand = new RelayCommand(Borrar);
                }
                return borrarCommand;
            }
        }
        private async void CargarMapaCommand(object sender, RoutedEventArgs e)
        {
            //SetMapStyle();
            //mapIconAddButton_Click();
            //SetMapProjection();
        }

        public string  MapServiceToken
        {
            get { return _mapServiceToken; }
            set
            {
                if (value != _mapServiceToken)
                {
                    _mapServiceToken = value;
                    RaisePropertyChanged(() => MapServiceToken);
                }
            }
        }
        public Geopoint GeoUbicacion
        {
            get { return _geoUbicacion; }
            set
            {
                if (value != _geoUbicacion)
                {
                    _geoUbicacion = value;
                    RaisePropertyChanged(() => GeoUbicacion);
                }
            }
        }
        public int Zoom
        {
            get { return _zoom; }
            set
            {
                if (value != _zoom)
                {
                    _zoom = value;
                    RaisePropertyChanged(() => Zoom);
                }
            }
        }
        public MapStyle StyleMap
        {
            get { return _styleMap; }
            set
            {
                if (value != _styleMap)
                {
                    _styleMap = value;
                    RaisePropertyChanged(() => StyleMap);
                }
            }
        }
        

        public string NombreConsultorio
        {
            get { return _nombreConsultorio; }
            set
            {
                if (value != _nombreConsultorio)
                {
                    _nombreConsultorio = value;
                    RaisePropertyChanged(() => NombreConsultorio);
                }
            }
        }

        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if (value != _nombre)
                {
                    _nombre = value;
                    RaisePropertyChanged(() => Nombre);
                }
            }
        }
        public string Direccion
        {
            get { return _direccion; }
            set
            {
                if (value != _direccion)
                {
                    _direccion = value;
                    RaisePropertyChanged(() => Direccion);
                }
            }
        }
        public string Encargado
        {
            get { return _encargado; }
            set
            {
                if (value != _encargado)
                {
                    _encargado = value;
                    RaisePropertyChanged(() => Encargado);
                }
            }
        }

        public bool EstadoNuevo
        {
            get { return _estadoNuevo; }
            set
            {
                if (value != _estadoNuevo)
                {
                    _estadoNuevo = value;
                    RaisePropertyChanged(() => EstadoNuevo);
                }
            }
        }
        public bool ActivoNuevo
        {
            get { return _activoNuevo; }
            set
            {
                if (value != _activoNuevo)
                {
                    _activoNuevo = value;
                    RaisePropertyChanged(() => ActivoNuevo);
                }
            }
        }

        public bool EditarTexto
        {
            get { return _editarTexto; }
            set
            {
                if (value != _editarTexto)
                {
                    _editarTexto = value;
                    RaisePropertyChanged(() => EditarTexto);

                }
            }
        }
        public ObservableCollection<Db_Consultorio> ListaConsultorio
        {
            get { return _listaConsultorio; }
            set
            {
                if (value != _listaConsultorio)
                {
                    _listaConsultorio = value;
                    RaisePropertyChanged(() => ListaConsultorio);
                }
            }
        }
        public Db_Consultorio ConsultorioLoc
        {
            get { return _consultorio; }
            set
            {
                if (value != _consultorio)
                {
                    _consultorio = value;
                    CargarDatos(_consultorio);
                    RaisePropertyChanged(() => ConsultorioLoc);
                }
            }
        }

        public ConsultorioViewModel(IBusinessLayer BusinessLayer)
        {
            _BusinessLayer = BusinessLayer;
            ListaConsultorio = _BusinessLayer.DameListaConsultorio();
            EditarTexto = false;
            EstadoNuevo = true;
            ActivoNuevo = true;

        }


        public void PageLoaded()
        {
            NombreConsultorio = "Actual";
            ListaConsultorio = _BusinessLayer.DameListaConsultorio();
            CargarMapa();

        }
        private async void CargarMapa()
        {
            mapIconStreamReference = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/mapicon.png"));
            var loader = new ResourceLoader();
            MapServiceToken = loader.GetString("MapServiceToken");
            if (_BusinessLayer.HayInternet())
            {
              await UbicacionActual(); 
            }
            StyleMap = SetMapStyle(1);
        }

        private async Task UbicacionActual()
        {
            Geolocator geolocator = new Geolocator();
            Geoposition pos = await geolocator.GetGeopositionAsync();
            Geopoint myLocation = pos.Coordinate.Point;
            RecargarPunto?.Invoke(this, new Types.Ubicacion { ubicacion = myLocation, nombre = NombreConsultorio, Zoom = 14 });
            GeoUbicacion = myLocation;

        }
        private  void Ubicacion(double Lat , double Lon)
        {
            // BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = 47.604, Longitude = -122.329 };
            BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = Lat, Longitude = Lon };
            Geopoint cityCenter = new Geopoint(cityPosition);
            GeoUbicacion = cityCenter;
        }
        private MapStyle SetMapStyle(int styleCombobox)
        {
            switch (styleCombobox)
            {
                case 0:
                    return MapStyle.None;
                case 1:
                    return MapStyle.Road;
                case 2:
                    return MapStyle.Aerial;
                case 3:
                    return MapStyle.AerialWithRoads;
                case 4:
                    return MapStyle.Terrain;
            }
            return MapStyle.None;
        }

        private async void Guardar()
        {
            try
            {
                if (Nombre != null && Direccion != null && Encargado != null)
                {
                    if (!EstadoNuevo)
                    {
                        _BusinessLayer.GuardarNuevoConsultorio(new Db_Consultorio
                        {
                            Consultorio = Nombre,
                            Direccion =Direccion,
                            Encargado =Encargado,
                            Latitude = GeoUbicacion.Position.Latitude.ToString(),
                            Longitude = GeoUbicacion.Position.Longitude.ToString()
                        });
                        EstadoNuevo = false;

                    }
                    else
                    {
                        _consultorio.Latitude = Consultorio.UbicacionActual.ubicacion.Position.Latitude.ToString();
                        _consultorio.Longitude = Consultorio.UbicacionActual.ubicacion.Position.Longitude.ToString();
                        _consultorio.Consultorio = Nombre;
                        _consultorio.Direccion = Direccion;
                        _consultorio.Encargado = Encargado;
                        _BusinessLayer.GuardarConsultorio(_consultorio);
                    }
                    if (!EstadoNuevo)
                    {
                        EstadoNuevo = true;
                    }
                    if (!ActivoNuevo)
                    {
                        ActivoNuevo = true;
                    }
                    Restablecer();
                    ListaConsultorio = _BusinessLayer.DameListaConsultorio();
                 }
                
                else
                {
                    MessageDialog dialog = new MessageDialog(
                                                   "Es nesesario llenar los datos requeridos", "Faltan Datos");
                    dialog.Commands.Add(new UICommand("Continuar"));
                    IUICommand command = await dialog.ShowAsync();
                }

            }
            catch (Exception ex)
            {

                Debug.WriteLine("Error CargarDatos");
            }

        }
        private void Nuevo()
        {
            if (EstadoNuevo)
            {

                Restablecer();
                EditarTexto = true;
                EstadoNuevo = false;
            }
            else
            {
                Restablecer();
                EstadoNuevo = true;

            }

        }
        private void Modificar()
        {
            if (EditarTexto)
            {
                ActivoNuevo = true;
                EditarTexto = false;
            }
            else
            {
                EditarTexto = true;
                ActivoNuevo = false;
            }


        }
        private async void Borrar()
        {

            if (ConsultorioLoc != null)
            {
                if (ConsultorioLoc.Id != 1)
                {
                    MessageDialog dialog = new MessageDialog(
                                               "Desea continuar con la operacion?", "Esta apunto de borrar al usuario: " + ConsultorioLoc.Consultorio + ".");
                    dialog.Commands.Add(new UICommand("Continuar"));
                    dialog.Commands.Add(new UICommand("Cancelar"));
                    IUICommand command = await dialog.ShowAsync();
                    if (command.Label.Equals("Continuar", StringComparison.CurrentCultureIgnoreCase))
                    {
                        _BusinessLayer.BorrarConsultorio(ConsultorioLoc);
                        Restablecer();
                        ListaConsultorio = _BusinessLayer.DameListaConsultorio();
                    }
                }
                else
                {
                    MessageDialog dialog = new MessageDialog(
                                               "No se puede borrar al super administrador", "Operacion incorrecta");
                    dialog.Commands.Add(new UICommand("Continuar"));
                    IUICommand command = await dialog.ShowAsync();
                }


            }

        }
        private void CargarDatos(Db_Consultorio ConsultorioCargar)
        {
            try
            {
                if (!EstadoNuevo)
                {
                    EstadoNuevo = true;
                }
                if (!ActivoNuevo)
                {
                    ActivoNuevo = true;
                }
                if (EditarTexto)
                {
                    EditarTexto = false;
                }
                if (ConsultorioCargar != null)
                {
                    Nombre = ConsultorioCargar.Consultorio;
                    Direccion= ConsultorioCargar.Direccion;
                    Encargado = ConsultorioCargar.Encargado;


                }
                if (ConsultorioCargar != null)
                {
                    Ubicacion(Convert.ToDouble(ConsultorioCargar.Latitude), Convert.ToDouble(ConsultorioCargar.Longitude));
                    RecargarPunto?.Invoke(this, new Types.Ubicacion { ubicacion = GeoUbicacion, nombre = ConsultorioCargar.Consultorio, Zoom = 14 });
                }
            }
            catch (Exception ex)
            {

                Debug.WriteLine("Error CargarDatos");
            }

        }

        
        private async void Restablecer()
        {
            Nombre = null;
            Direccion = null;
            Encargado = null;
            EditarTexto = false;
            await UbicacionActual();
        }


    }
}

//catch (Exception ex)
//            {

//                Debug.WriteLine("Error CargarDatos");
//            }

