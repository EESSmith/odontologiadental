﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using OdontologiaDental.Data.TypesTablas;
using OdontologiaDental.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace OdontologiaDental.ViewModel
{
    public class UsuariosViewModel : ViewModelBase
    {
        private IBusinessLayer _BusinessLayer;
        private ObservableCollection<Db_Usuario> _listaUsuarios;
        private ObservableCollection<Db_Consultorio> _listaConsultorio;
        private ObservableCollection<Db_Permisos> _listaPermiso;

        private string _usuario;
        private string _nombre;
        private string _apellido;
        private string _telefono;
        private string _contra;
        private string _contraR;
        private string _correo;
        private string _imagen;
        private int _numUsuarios;
        private Db_Usuario _usuarioSelect;
        private Db_Permisos _permiso;
        private Db_Consultorio _consultorio;        
        private Db_Huella _huella;
        private bool _editarTexto;

        private ICommand guardarCommand;
        private ICommand nuevoCommand;
        private ICommand modificarCommand;
        private ICommand borrarCommand;

        private ICommand pageLoaded;

        

        private bool _estadoNuevo;
        private bool _activoNuevo;




        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if (value != _nombre)
                {
                    _nombre = value;
                    RaisePropertyChanged(() => Nombre);
                }
            }
        }
        public string Apellido
        {
            get { return _apellido; }
            set
            {
                if (value != _apellido)
                {
                    _apellido = value;
                    RaisePropertyChanged(() => Apellido);
                }
            }
        }
        public string Telefono
        {
            get { return _telefono; }
            set
            {
                if (value != _telefono)
                {
                    _telefono = value;
                    RaisePropertyChanged(() => Telefono);
                }
            }
        }
        public string Correo
        {
            get { return _correo; }
            set
            {
                if (value != _correo)
                {
                    _correo = value;
                    RaisePropertyChanged(() => Correo);
                }
            }
        }
        public Db_Consultorio Consultorio
        {
            get { return _consultorio; }
            set
            {
                if (value != _consultorio)
                {
                    _consultorio = value;
                    RaisePropertyChanged(() => Consultorio);
                }
            }
        }
        public string Usuario
        {
            get { return _usuario; }
            set
            {
                if (value != _usuario)
                {
                    _usuario = value;
                    RaisePropertyChanged(() => Usuario);
                }
            }
        }
        public string Contra
        {
            get { return _contra; }
            set
            {
                if (value != _contra)
                {
                    _contra = value;
                    RaisePropertyChanged(() => Contra);
                }
            }
        }
        public string ContraR
        {
            get { return _contraR; }
            set
            {
                if (value != _contraR)
                {
                    _contraR = value;
                    RaisePropertyChanged(() => ContraR);
                }
            }
        }
        public Db_Permisos Permiso
        {
            get { return _permiso; }
            set
            {
                if (value != _permiso)
                {
                    _permiso = value;
                    RaisePropertyChanged(() => Permiso);
                }
            }
        }
        public int NumUsuarios
        {
            get { return _numUsuarios; }
            set
            {
                if (value != _numUsuarios)
                {
                    _numUsuarios = value;
                    RaisePropertyChanged(() => NumUsuarios);
                }
            }
        }
        public Db_Usuario UsuarioSelect
        {
            get { return _usuarioSelect; }
            set
            {
                if (value != _usuarioSelect)
                {
                    _usuarioSelect = value;
                    CargarDatos(_usuarioSelect);
                    RaisePropertyChanged(() => UsuarioSelect);

                }
            }
        }
        public bool EstadoNuevo
        {
            get { return _estadoNuevo; }
            set
            {
                if (value != _estadoNuevo)
                {
                    _estadoNuevo = value;
                    RaisePropertyChanged(() => EstadoNuevo);
                }
            }
        }
        public bool ActivoNuevo
        {
            get { return _activoNuevo; }
            set
            {
                if (value != _activoNuevo)
                {
                    _activoNuevo = value;
                    RaisePropertyChanged(() => ActivoNuevo);
                }
            }
        }

        public bool EditarTexto
        {
            get { return _editarTexto; }
            set
            {
                if (value != _editarTexto)
                {
                    _editarTexto = value;
                    RaisePropertyChanged(() => EditarTexto);

                }
            }
        }


        public ObservableCollection<Db_Permisos> ListaPermiso
        {
            get { return _listaPermiso; }
            set
            {
                if (value != _listaPermiso)
                {
                    _listaPermiso = value;
                    RaisePropertyChanged(() => ListaPermiso);
                }
            }
        }
        public ObservableCollection<Db_Consultorio> ListaConsultorio
        {
            get { return _listaConsultorio; }
            set
            {
                if (value != _listaConsultorio)
                {
                    _listaConsultorio = value;
                    RaisePropertyChanged(() => ListaConsultorio);
                }
            }
        }
        public ObservableCollection<Db_Usuario> ListaUsuarios
        {
            get { return _listaUsuarios; }
            set
            {
                if (value != _listaUsuarios)
                {
                    _listaUsuarios = value;
                    RaisePropertyChanged(() => ListaUsuarios);
                }
            }
        }

        public ICommand GuardarCommand
        {
            get
            {
                if (guardarCommand == null)
                {
                    guardarCommand = new RelayCommand(Guardar);
                }
                return guardarCommand;
            }
        }
        public ICommand NuevoCommand
        {
            get
            {
                if (nuevoCommand == null)
                {
                    nuevoCommand = new RelayCommand(Nuevo);
                }
                return nuevoCommand;
            }
        }
        public ICommand ModificarCommand
        {
            get
            {
                if (modificarCommand == null)
                {
                    modificarCommand = new RelayCommand(Modificar);
                }
                return modificarCommand;
            }
        }
        public ICommand BorrarCommand
        {
            get
            {
                if (borrarCommand == null)
                {
                    borrarCommand = new RelayCommand(Borrar);
                }
                return borrarCommand;
            }
        }
        public ICommand PageLoadedCommand
        {
            get
            {
                if (pageLoaded == null)
                {
                    pageLoaded = new RelayCommand(PageLoaded);
                }
                return pageLoaded;
            }
        }



        public UsuariosViewModel(IBusinessLayer BusinessLayer)
        {
            _BusinessLayer = BusinessLayer;
            ListaUsuarios = _BusinessLayer.DameListaUsuarios();
            NumUsuarios = ListaUsuarios.Count;
            ListaPermiso = _BusinessLayer.DameListaPermisos();
            ListaConsultorio = _BusinessLayer.DameListaConsultorio();
            EditarTexto = false;
            EstadoNuevo = true;
            ActivoNuevo = true;
        }

        public void PageLoaded()
        {
            ListaConsultorio = _BusinessLayer.DameListaConsultorio();
            ListaUsuarios = _BusinessLayer.DameListaUsuarios();
        }

        private async void Guardar()
        {
            try
            {
                if (Nombre != null && Correo != null && Permiso != null && Contra != null && Usuario != null) 
                {
                    if (Contra != null && ContraR != null && Contra == ContraR)
                    {

                        if (!EstadoNuevo)
                        {
                            _BusinessLayer.GuardarNuevoUsuario(new Db_Usuario
                            {
                                Nombre = Nombre,
                                Apellido = Apellido,
                                Telefono = Telefono,
                                Correo = Correo,
                                Consultorio = Consultorio,
                                Usuario = Usuario,
                                Contra = Contra,
                                PermisoId = Permiso.Id
                            });
                            EstadoNuevo = false;

                        }
                        else
                        {
                            _usuarioSelect.Nombre = Nombre;
                            _usuarioSelect.Apellido = Apellido;
                            _usuarioSelect.Telefono = Telefono;
                            _usuarioSelect.Correo = Correo;
                            _usuarioSelect.Consultorio = Consultorio;
                            _usuarioSelect.Usuario = Usuario;
                            _usuarioSelect.Contra = Contra;
                            _usuarioSelect.Permiso = Permiso;
                            _BusinessLayer.GuardarUsuario(_usuarioSelect);
                        }
                        if (!EstadoNuevo)
                        {
                            EstadoNuevo = true;
                        }
                        if (!ActivoNuevo)
                        {
                            ActivoNuevo = true;
                        }
                        Restablecer();
                        ListaUsuarios = _BusinessLayer.DameListaUsuarios();
                    }
                    else
                    {
                        MessageDialog dialog = new MessageDialog(
                                                   "La confirmacion de contraseña tiene que coincidir", "No coinciden las contraseñas");
                        dialog.Commands.Add(new UICommand("Continuar"));
                        IUICommand command = await dialog.ShowAsync();
                    }
                }
                else
                {
                    MessageDialog dialog = new MessageDialog(
                                                   "Es nesesario llenar los datos requeridos", "Faltan Datos");
                    dialog.Commands.Add(new UICommand("Continuar"));
                    IUICommand command = await dialog.ShowAsync();
                }

            }
            catch (Exception ex)
            {

                Debug.WriteLine("Error CargarDatos");
            }

        }
        private void Nuevo()
        {
            if (EstadoNuevo)
            {
                
                Restablecer();
                EditarTexto = true;
                EstadoNuevo = false;
            }
            else
            {
                Restablecer();
                EstadoNuevo = true;
                
            }
            
        }
        private void Modificar()
        {
            if (EditarTexto)
            {
                ActivoNuevo = true;
                EditarTexto = false;
            } else {
                EditarTexto = true;
                ActivoNuevo = false; }


        }
        private async void Borrar()
        {

            if (UsuarioSelect!=null)
            {
                if (UsuarioSelect.Id!=1)
                {
                    MessageDialog dialog = new MessageDialog(
                                               "Desea continuar con la operacion?", "Esta apunto de borrar al usuario: " + UsuarioSelect.Nombre + ".");
                    dialog.Commands.Add(new UICommand("Continuar"));
                    dialog.Commands.Add(new UICommand("Cancelar"));
                    IUICommand command = await dialog.ShowAsync();
                    if (command.Label.Equals("Continuar", StringComparison.CurrentCultureIgnoreCase))
                    {
                        _BusinessLayer.BorrarUsuario(UsuarioSelect);
                        Restablecer();
                        ListaUsuarios = _BusinessLayer.DameListaUsuarios();
                    }
                }
                else
                {
                    MessageDialog dialog = new MessageDialog(
                                               "No se puede borrar al super administrador", "Operacion incorrecta");
                    dialog.Commands.Add(new UICommand("Continuar"));
                    IUICommand command = await dialog.ShowAsync();
                }
                
                
            }

        }
        private void CargarDatos(Db_Usuario usuarioCargar)
        {
            try
            {
                if (!EstadoNuevo)
                {
                    EstadoNuevo = true;
                }
                if (!ActivoNuevo)
                {
                    ActivoNuevo = true;
                }
                if (EditarTexto)
                {
                    EditarTexto = false;
                }
                if (usuarioCargar != null)
                {
                    Nombre = usuarioCargar.Nombre;
                    Apellido = usuarioCargar.Apellido;
                    Telefono = usuarioCargar.Telefono;
                    Correo = usuarioCargar.Correo;
                    Consultorio = usuarioCargar.Consultorio;                    
                    Usuario = usuarioCargar.Usuario;
                    Contra = usuarioCargar.Contra;
                    ContraR = usuarioCargar.Contra;
                    Permiso = usuarioCargar.Permiso;
                    Consultorio = usuarioCargar.Consultorio;

                }
               
            }
            catch (Exception ex)
            {

                Debug.WriteLine("Error CargarDatos");
            }

        }
        private void Restablecer()
        {
            Nombre = null;
            Apellido = null;
            Telefono = null;
            Correo = null;
            Consultorio = null;
            Usuario = null;
            Contra = null;
            ContraR = null;
            Permiso = null;
            UsuarioSelect = null;
            EditarTexto = false;
        }

    }
}
