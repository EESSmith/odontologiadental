﻿using System.Threading.Tasks;
using OdontologiaDental.Model;

namespace OdontologiaDental.Design
{
    public class DesignBusinessLayer 
    {
        public Task<DataItem> GetData()
        {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            return Task.FromResult(item);
        }
    }
}