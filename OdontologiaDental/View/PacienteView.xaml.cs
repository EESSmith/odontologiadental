﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Resources;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=234238

namespace OdontologiaDental.View
{
    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class PacienteView : Page
    {
        public double GetWidthPanelPrincipal
        {
            get
            {
                return this.PanelPrincipal.ActualWidth;
            }
        }
        public PacienteView()
        {
            this.InitializeComponent();

            IList<int> edad = new List<int>();
            for (int i = 1; i < 120; i++)
            {
                edad.Add(i);
            }
            EdadTotal.ItemsSource = edad;


            //datoFiltra.TextChanged += DatoFiltra_TextChanged;
        }

        private void DatoFiltra_TextChanged(object sender, TextChangedEventArgs e)
        {
            //var adta =datoFiltra.Text;

        }
    }
}
