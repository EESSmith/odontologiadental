﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.ApplicationModel.Resources;
using System.Diagnostics;
using Windows.Storage.Streams;
using OdontologiaDental.ViewModel;
using OdontologiaDental.Types;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=234238

namespace OdontologiaDental.View
{
    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class Consultorio : Page
    {
        RandomAccessStreamReference mapIconStreamReference;
        public static Ubicacion UbicacionActual;


        public Consultorio()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Disabled;
            mapIconStreamReference = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/mapicon.png"));
            ConsultorioViewModel.RecargarPunto += ConsultorioViewModel_RecargarPunto;
           
        }

        private void ConsultorioViewModel_RecargarPunto(object sender, Ubicacion e)
        {
            UbicacionActual = e;
            mapIconAddButton_Click(e);
        }
        

        private void MyMap_MapTapped(Windows.UI.Xaml.Controls.Maps.MapControl sender, Windows.UI.Xaml.Controls.Maps.MapInputEventArgs args)
        {
            var tappedGeoPosition = args.Location.Position;
            string status = "MapTapped at \nLatitude:" + tappedGeoPosition.Latitude + "\nLongitude: " + tappedGeoPosition.Longitude;
            Debug.WriteLine("MapTapped at \nLatitude:" + tappedGeoPosition.Latitude + "\nLongitude: " + tappedGeoPosition.Longitude);
            if (UbicacionActual!=null)
            {
                UbicacionActual.ubicacion = Ubicacion(tappedGeoPosition.Latitude, tappedGeoPosition.Longitude);
                myMap.MapElements.Clear();
                CambiarPosicion(UbicacionActual);
            }
            
        }



        private void mapIconAddButton_Click(Ubicacion e)
        {
            try
            {
                myMap.MapElements.Clear();
                MapIcon mapIcon1 = new MapIcon();
                mapIcon1.Location = e.ubicacion;
                mapIcon1.NormalizedAnchorPoint = new Point(0.5, 1.0);
                mapIcon1.Title = e.nombre;
                mapIcon1.Image = mapIconStreamReference;
                mapIcon1.ZIndex = 0;
                myMap.MapElements.Add(mapIcon1);
                myMap.ZoomLevel = e.Zoom;
                myMap.Center = e.ubicacion;
            }
            catch (Exception ex)
            {

            }
           
        }
        private void CambiarPosicion(Ubicacion e)
        {
            try
            {
                MapIcon mapIcon1 = new MapIcon();
                mapIcon1.Location = e.ubicacion;
                mapIcon1.NormalizedAnchorPoint = new Point(0.5, 1.0);
                mapIcon1.Title = e.nombre;
                mapIcon1.Image = mapIconStreamReference;
                mapIcon1.ZIndex = 0;
                myMap.MapElements.Add(mapIcon1);
                myMap.Center = e.ubicacion;
            }
            catch (Exception ex)
            {

            }

        }
        private Geopoint Ubicacion(double Lat, double Lon)
        {
            // BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = 47.604, Longitude = -122.329 };
            BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = Lat, Longitude = Lon };
            Geopoint cityCenter = new Geopoint(cityPosition);
            return cityCenter;
        }
    }
}
