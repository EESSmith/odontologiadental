﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace OdontologiaDental.Types
{
    public class Ubicacion
    {
        public string nombre { get; set; }
        public Geopoint ubicacion{get; set;}

        public int Zoom { get; set; }
    }
}
