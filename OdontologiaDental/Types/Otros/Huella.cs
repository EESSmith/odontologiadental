﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Huella
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string huella { get; set; }
        public string Creacion { get; set; }
        public string Modificacion { get; set; }
    }
}
