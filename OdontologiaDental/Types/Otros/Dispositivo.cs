﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Dispositivo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Version { get; set; }
        public string Serie { get; set; }
        public string Mac { get; set; }
    }
}
