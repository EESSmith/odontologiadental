﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class ExamenDental
    {
        public int Id { get; set; }
        public string Fecha { get; set; }
        public int Diente { get; set; }
        public string Diagnostico { get; set; }
        public float Costo { get; set; }
        public float Pago { get; set; }
        public float Diferencia { get; set; }
        public string Autorizacion { get; set; }
        public Usuario Usuario { get; set; }
    }
}
