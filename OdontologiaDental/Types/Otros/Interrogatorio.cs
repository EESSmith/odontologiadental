﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Interrogatorio
    {
        public int Id { get; set; }
        public string Fecha { get; set; }
        public string Pregunta { get; set; }
        public int NumeroPregunta { get; set; }
        public string Respuesta { get; set; } 
        public bool? SiNoRespuesta { get; set; } = false;

    }
}
