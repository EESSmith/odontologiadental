﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Examen
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Fecha { get; set; }
        public Usuario Usuario { get; set; }
        public Paciente Paciente { get; set; }
        public ExamenFacial Examen_Facial { get; set; }
        public ExamenDental Examen_Dental { get; set; }
        public List<Interrogatorio> Interrogatorio { get; set; }

    }
}
