﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Especialista
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string especialista { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Vinculo { get; set; }
        public string Especialidad { get; set; }
        public string Fecha { get; set; }
        public Usuario Usuario { get; set; }
        public string Egresado { get; set; }
        public string Nacimiento { get; set; }
        public string Rfc { get; set; }
        //public Consultorio Consultorio { get; set; }
    }
}
