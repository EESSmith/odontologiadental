﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Paciente
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public int Expediente { get; set; }
        public string Fecha { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Sexo { get; set; }
        public string Lugar_Nacimiento { get; set; }
        public string Fecha_Nacimiento { get; set; }
        public string Direccion { get; set; }
        public string Ocupacion { get; set; }
        public int Estado_civil { get; set; }
        public string Telefono_Oficina { get; set; }
        public string Telefono_Casa { get; set; }
        public string Telefono_Celular { get; set; }
        public string Referencia { get; set; }
        public string Relacion { get; set; }
        public string Tutor { get; set; }
        public Usuario Usuario { get; set; }
        public Huella Huella { get; set; }
        public string Imagen { get; set; }
        public Examen Examen { get; set; }
    }
}
