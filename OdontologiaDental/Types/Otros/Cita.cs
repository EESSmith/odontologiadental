﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Cita
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Creacion { get; set; }
        public string Fecha { get; set; }
        public string Asunto { get; set; }
        public int Prioridad { get; set; }
        public int Activa { get; set; }
        //public Consultorio Consultorio { get; set; }
        public Especialista Especialista { get; set; }
        public Usuario Usuario { get; set; }
        public Paciente Paciente { get; set; }
    }
}
