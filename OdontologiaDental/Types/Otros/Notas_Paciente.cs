﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Notas_Paciente
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Fecha { get; set; }
        public string Observaciones { get; set; }
    }
}
