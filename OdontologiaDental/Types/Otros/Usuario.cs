﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Usuario
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string usuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Contra { get; set; }
        public Permisos Permiso { get; set; }
        public Huella Huella { get; set; }
        public string Fecha { get; set; }
        public string Imagen { get; set; }
        public byte[] Timestamp { get; set; }
    }    
}
