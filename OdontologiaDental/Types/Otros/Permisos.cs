﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdontologiaDental.Types2
{
    public class Permisos
    {
        public int Id { get; set; }
        public int Estado { get; set; }
        public string Nombre { get; set; }

    }
}
