﻿using OdontologiaDental.Data.TypesTablas;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace OdontologiaDental.Model
{
    public interface IBusinessLayer
    {
        Db_Usuario UsuarioActual { get; }
        Task<DataItem> GetData();
        bool HayInternet();
        bool IniciarSesion(string usuario, string password); //comprueba el inicio de sesion
        bool CerrarSesion(bool salida);//Cierra cession

        ObservableCollection<Db_Usuario> DameListaUsuarios();
        ObservableCollection<Db_Permisos> DameListaPermisos();
        ObservableCollection<Db_Consultorio> DameListaConsultorio();

        bool GuardarUsuario(Db_Usuario Usuario);
        bool GuardarNuevoUsuario(Db_Usuario Usuario);
        bool BorrarUsuario(Db_Usuario Usuario);

        bool GuardarConsultorio(Db_Consultorio Consultorio);
        bool GuardarNuevoConsultorio(Db_Consultorio Consultorio);
        bool BorrarConsultorio(Db_Consultorio Consultorio);





    }
}