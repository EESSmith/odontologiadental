﻿using GalaSoft.MvvmLight.Views;
using OdontologiaDental.Data;
using OdontologiaDental.Data.TypesTablas;
using OdontologiaDental.Util;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.UI.Xaml.Resources;

namespace OdontologiaDental.Model
{
    public partial class BusinessLayer : IBusinessLayer
    {

        #region "Variables"
        private IActualesRepository actualesRepository;
        private readonly INavigationService navigationService;



        public Db_Usuario UsuarioActual { get; set; }

        #endregion
        #region "Eventos"
        #endregion
        #region "Constructor"
        public BusinessLayer(IActualesRepository _actualesRepository, INavigationService _navigationService)
        {
            //"en-US"
            CustomXamlResourceLoader.Current = new CustomResourseDataLabel();
            navigationService = _navigationService;
            actualesRepository = _actualesRepository;
        }

        #endregion
        #region "Interno"
        public Task<DataItem> GetData()
        {
            var item = new DataItem("Welcome to MVVM Light");
            return Task.FromResult(item);
        }

        public bool IniciarSesion(string usuario, string password)
        {
            try
            {
                UsuarioActual = actualesRepository.InicioUsuario(usuario, password);
                if (UsuarioActual.Id != 0)
                {
                    navigationService.NavigateTo("Principal");
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool CerrarSesion(bool salida)
        {
            try
            {
                if (salida)
                {
                    UsuarioActual = null;
                    navigationService.GoBack();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public ObservableCollection<Db_Usuario> DameListaUsuarios()
        {
            return actualesRepository.DameUsuarios();
        }
        public ObservableCollection<Db_Permisos> DameListaPermisos()
        {

            return actualesRepository.DamePermisos();
        }
        public ObservableCollection<Db_Consultorio> DameListaConsultorio()
        {
            return actualesRepository.DameListaConsultorios();
        }

        public bool GuardarUsuario(Db_Usuario Usuario)
        {
            return actualesRepository.GuardarUsuario(Usuario);
        }
        public bool GuardarNuevoUsuario(Db_Usuario Usuario)
        {
            return actualesRepository.GuardarNuevoUsuario(Usuario);
        }
        public bool BorrarUsuario(Db_Usuario Usuario)
        {
            return actualesRepository.BorrarUsuario(Usuario);
        }


        public bool GuardarConsultorio(Db_Consultorio _Consultorio)
        {
            return actualesRepository.GuardarConsultorio(_Consultorio);
        }
        public bool GuardarNuevoConsultorio(Db_Consultorio _Consultorio)
        {
            return actualesRepository.GuardarNuevoConsultorio(_Consultorio);
        }
        public bool BorrarConsultorio(Db_Consultorio _Consultorio)
        {
            return actualesRepository.BorrarConsultorio(_Consultorio);
        }
        public bool HayInternet()
        {
            var x = NetworkInformation.GetInternetConnectionProfile();
            var rr = x.GetNetworkConnectivityLevel();
            if (x != null)
            {
                //LoggingServices.Instance.WriteLine<BackgroundActivity>("Si hay internet", MetroLog.LogLevel.Info);
                return true;
            }
            else
            {
                // LoggingServices.Instance.WriteLine<BackgroundActivity>("No hay internet", MetroLog.LogLevel.Info);
                return false;
            }
        }
        #endregion
        #region "Externo"
        #endregion



    }
}


#region "Variables"
#endregion
#region "Eventos"
#endregion
#region "Constructor"
#endregion
#region "Interno"
#endregion
#region "Externo"
#endregion
